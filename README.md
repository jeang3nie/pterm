This is an exploration of coding a Gtk+ application in Nim using Gintro.

## Building
Make sure you have the nim toolchain installed, along with Gtk+-3.x and Vte
libraries and headers.
```Sh
# Install Gintro
nimble install gintro
# Build the program
nimble build
```