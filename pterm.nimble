# Package

version       = "0.1.0"
author        = "Nathan Fisher"
description   = "A terminal emulator for Puppy Linux"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["pterm"]


# Dependencies

requires "nim >= 1.6.6"
requires "gintro"
