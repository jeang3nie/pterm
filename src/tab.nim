import options
import gintro/[gtk, glib, gobject, vte]
import terminal

# Subclass a Paned widget as a Tab, which retains a reference to
# it's label widgets and internal panes
type
    Tab* = ref object of Paned
        lbox*: Box               # The box containing the tab label and close button
        label*: Label            # The actual label
        closeButton*: Button     # Close button on the tab label
        p1: Paned                # 1st pane
        p2: Paned                # 2nd pane
        terminals*: int          # Total number of terminals in this tab
        slot_aa: Option[PTerm]   # 1st child of 1st pane
        slot_ab: Option[PTerm]   # 2nd child of 1st pane
        slot_ba: Option[PTerm]   # 1st child of 2nd pane
        slot_bb: Option[PTerm]   # 2nd child of 2nd pane
        focus: Slot              # The most recently focused slot

when defined(gcDestructors):
    proc `=destroy`(x: var typeof(Tab()[])) =
        gtk.`=destroy`(typeof(Paned()[])(x))

# If we aren't specifying a command to run then
# fall back to the user's shell
let environ = getEnviron()
let command = environ.environGetenv("SHELL")

# Called when the child process of a terminal exits to
# close the terminal and clean up
proc closeTerm(terminal: Terminal, n: int, p: Paned) =
    let tab = Tab(p)
    let pterm = PTerm(terminal)
    case pterm.slot:
    of Slot.aa:
        tab.p1.remove(pterm)
        tab.slot_aa = none(PTerm)
    of Slot.ab:
        tab.p1.remove(pterm)
        tab.slot_ab = none(PTerm)
    of Slot.ba:
        tab.p2.remove(pterm)
        tab.slot_ba = none(PTerm)
    of Slot.bb:
        tab.p2.remove(pterm)
        tab.slot_bb = none(PTerm)
    tab.terminals -= 1
    if tab.terminals == 0:
        tab.destroy()

# Add a terminal to the tab
proc initTerm(tab: Tab, slot: Slot, cmd: string): Terminal =
    let term = newPTerm(slot)
    result = term
    var pid = 0
    discard term.spawnSync({}, nil, [cmd], [], {SpawnFlag.leaveDescriptorsOpen}, nil, nil, pid, nil)
    case slot
    of Slot.aa:
        tab.p1.pack1(term, true, true)
        tab.slot_aa = some(term)
    of Slot.ab:
        tab.p1.pack2(term, true, true)
        tab.slot_ab = some(term)
    of Slot.ba:
        tab.p2.pack1(term, true, true)
        tab.slot_ba = some(term)
    of Slot.bb:
        tab.p2.pack2(term, true, true)
        tab.slot_bb = some(term)
    tab.terminals += 1
    Terminal(term).connect("child-exited", closeTerm, Paned(tab))

proc split*(tab: Tab) =
    case tab.focus
    of Slot.aa:
        if tab.slot_ab.isNone():
            let term = tab.initTerm(ab, command)
            term.show()
        elif tab.slot_ba.isNone():
            tab.pack2(tab.p2, true, true)
            let term = tab.initTerm(ba, command)
        elif tab.slot_bb.isNone():
            tab.pack2(tab.p2, true, true)
            let term = tab.initTerm(bb, command)
    of Slot.ab:
        if tab.slot_aa.isNone():
            discard tab.initTerm(aa, command)
        elif tab.slot_ba.isNone():
            tab.pack2(tab.p2, true, true)
            discard tab.initTerm(ba, command)
        elif tab.slot_bb.isNone():
            tab.pack2(tab.p2, true, true)
            discard tab.initTerm(bb, command)
    of Slot.ba:
        if tab.slot_bb.isNone():
            discard tab.initTerm(bb, command)
        elif tab.slot_aa.isNone():
            discard tab.initTerm(aa, command)
        elif tab.slot_ab.isNone():
            discard tab.initTerm(ab, command)
    of Slot.bb:
        if tab.slot_ba.isNone():
            discard tab.initTerm(ba, command)
        elif tab.slot_aa.isNone():
            discard tab.initTerm(aa, command)
        elif tab.slot_ab.isNone():
            discard tab.initTerm(ab, command)
    tab.showAll()

proc rotate*(tab: Tab) =
    if tab.orientation == Orientation.horizontal:
        tab.setOrientation(Orientation.vertical)
        tab.p1.setOrientation(Orientation.horizontal)
        tab.p2.setOrientation(Orientation.horizontal)
    else:
        tab.setOrientation(Orientation.horizontal)
        tab.p1.setOrientation(Orientation.vertical)
        tab.p2.setOrientation(Orientation.vertical)

# Constructor
proc newTab*(cmd: string = command): Tab =
    let tab = newPaned(Tab, Orientation.horizontal)
    result = tab
    tab.lbox = newBox(Orientation.horizontal)
    tab.label = newLabel(cstring(command))
    tab.lbox.packStart(tab.label, true, true, 0)
    tab.closeButton = newButton()
    tab.closeButton.setImage(newImageFromIconName(cstring"window-close-symbolic", 1))
    tab.closeButton.setRelief(ReliefStyle.none)
    tab.lbox.packEnd(tab.closeButton, false, false, 0)
    tab.lbox.show_all()
    tab.p1 = newPaned(Orientation.vertical)
    tab.pack1(tab.p1, true, true)
    tab.p2 = newPaned(Orientation.vertical)
    tab.terminals = 0
    discard tab.initTerm(aa, cmd)
    tab.slot_aa = none(PTerm)
    tab.slot_bb = none(PTerm)
    tab.slot_ba = none(PTerm)
    tab.slot_bb = none(PTerm)
    tab.focus = aa
