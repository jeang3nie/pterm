import gintro/[gtk, gobject, gio, vte]
import window

proc quitApp(t: Terminal, n: int, app: Application) =
    quit(app)

proc appActivate(app: Application) =
    let window = newPtermWindow(app)
    window.title = "Pterm"
    window.defaultSize = (600, 400)
    #let terminal = newTerminal()
    #terminal.connect("child-exited", quitApp, app)
    showAll(window)

proc main =
    let app = newApplication("org.codeberg.jeang3nie.pterm")
    connect(app, "activate", appActivate)
    discard run(app)

when isMainModule:
    main()
