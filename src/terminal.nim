import gintro/[gobject, vte]

type
    Slot* = enum
        aa, ab, ba, bb

type
    PTerm* = ref object of Terminal
        slot*: Slot

when defined(gcDestructors):
    proc `=destroy`(x: var typeof(Tab()[])) =
        gtk.`=destroy`(typeof(Paned()[])(x))

proc newPTerm*(slot: Slot): PTerm = 
    let term = newTerminal(PTerm)
    result = term
    term.slot = slot