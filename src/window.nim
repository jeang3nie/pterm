import gintro/[gtk, gio, glib, gobject]
import tab

# The MenuModel definition for our hamburger menu
const menuData = """
<interface>
  <menu id="appMenu">
    <section>
      <item>
        <attribute name="label">New Tab</attribute>
        <attribute name="action">win.new-tab</attribute>
      </item>
      <item>
        <attribute name="label">Split Tab</attribute>
        <attribute name="action">win.split-tab</attribute>
      </item>
      <item>
        <attribute name="label">Rotate Tab</attribute>
        <attribute name="action">win.rotate-tab</attribute>
      </item>
    </section>
    <section>
      <item>
        <attribute name="label">About</attribute>
        <attribute name="action">app.about</attribute>
      </item>
      <item>
        <attribute name="label">Preferences</attribute>
        <attribute name="action">app.preferences</attribute>
      </item>
    </section>
    <section>
      <item>
        <attribute name="label">Quit</attribute>
        <attribute name="action">win.quit</attribute>
      </item>
    </section>
  </menu>
</interface>
"""

# Create a subclass of an ApplicationWindow which retains a
# reference to the internal Notebook
type
    PtermWindow* = ref object of ApplicationWindow
        notebook*: Notebook

when defined(gcDestructors):
    proc `=destroy`(x: var typeof(PtermWindow()[])) =
        gtk.`=destroy`(typeof(ApplicationWindow()[])(x))

let environ = getEnviron()
let command = environ.environGetenv("SHELL")

# Runs automatically whenever a tab is closed. When the number of tabs
# reaches 0, the window will then close
proc onTabClose(notebook: Notebook, child: Widget, n: int, window: Window) =
    let num = notebook.getNPages()
    if num == 0:
        window.close()

# The callback which runs when the close button is clicked on a tab
proc closeTab(button: Button, data: (PtermWindow, Tab)) =
    let window = data[0]
    let tab = data[1]
    let num = window.notebook.pageNum(tab)
    window.notebook.removePage(num)

# Creates a new tab
proc addTab*(window: PtermWindow, cmd: string = command): Tab =
    let tab = newTab()
    result = tab
    discard window.notebook.append_page(tab, tab.lbox)
    tab.closeButton.connect("clicked", closeTab, (window, tab))

# The callback which runs when a new tab is requested
proc onNewTab(action: SimpleAction, v: Variant, window: Window) =
    let win = PtermWindow(window)
    let tab = newTab()
    tab.show_all()
    tab.lbox.show_all()
    discard win.notebook.append_page(tab, tab.lbox)
    tab.closeButton.connect("clicked", closeTab, (win, tab))

proc onSplitTab(action: SimpleAction, v: Variant, window: Window) =
    let win = PtermWindow(window)
    let num = win.notebook.getCurrentPage()
    let tab = Tab(win.notebook.getNthPage(num))
    tab.split()

proc onRotateTab(action: SimpleAction, v: Variant, window: Window) =
    let win = PtermWindow(window)
    let num = win.notebook.getCurrentPage()
    let tab = Tab(win.notebook.getNthPage(num))
    tab.rotate()

# Constructor for a new window
proc newPtermWindow*(app: Application): PtermWindow =
    let window = newApplicationWindow(PtermWindow, app)
    result = window
    let vbox = newBox(Orientation.vertical)
    window.add(vbox)
    window.notebook = newNotebook()
    vbox.packStart(window.notebook, true, true, 0)
    let menuButton = newMenuButton()
    let img = newImageFromIconName(cstring"format-justify-fill-symbolic", 1)
    menuButton.setImage(img)
    menuButton.setRelief(ReliefStyle.none)
    menuButton.show()
    window.notebook.setActionWidget(menuButton, PackType.end)
    window.notebook.connect("page-removed", onTabClose, Window(window))
    
    # Initiate an action group and actions to associate with menu items
    # and keyboard shortcuts
    let actionGroup = newSimpleActionGroup()
    # New tab creation
    let newTab = newSimpleAction("new-tab")
    discard newTab.connect("activate", onNewTab, Window(window))
    actionGroup.addAction(newTab)
    app.setAccelsForAction("win.new-tab", "<Control><Shift>T")
    # Split a tab
    let splitTab = newSimpleAction("split-tab")
    discard splitTab.connect("activate", onSplitTab, Window(window))
    actionGroup.addAction(splitTab)
    app.setAccelsForAction("win.split-tab", "<Control><Shift>S")
    # Rotate tab orientation
    let rotateTab = newSimpleAction("rotate-tab")
    discard rotateTab.connect("activate", onRotateTab, Window(window))
    actionGroup.addAction(rotateTab)
    app.setAccelsForAction("win.rotate-tab", "<Control><Shift>R")
    # Insert the action group into the window
    window.insertActionGroup("win", actionGroup)
    
    # Get the MenuModel for our hamburger menu
    let builder: Builder = newBuilderFromString(menuData)
    let menuModel = builder.getMenuModel("appMenu")
    let menu = newMenuFromModel(menuModel)
    menuButton.setPopup(menu)
    
    # Add our first tab
    discard window.addTab()